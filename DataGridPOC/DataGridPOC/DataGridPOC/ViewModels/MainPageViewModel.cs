﻿using DataGridPOC.Data;
using Prism.Commands;
using Prism.Mvvm;
 using System.Collections.ObjectModel; 

namespace DataGridPOC.ViewModels {
    public class MainPageViewModel : BindableBase {

        private string _title;
        public string Title {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        } 

        private ObservableCollection<OrderInfo> _orderInfoCollection;
        public ObservableCollection<OrderInfo> OrderInfoCollection {
            get { return _orderInfoCollection; }
            set { this._orderInfoCollection = value; }
        }

        public MainPageViewModel() {
            Title = "Data Grid";
            OrderInfoCollection = new ObservableCollection<OrderInfo> {
                new OrderInfo(1001, "Maria Anders", "Germany", "ALFKI", "Berlin"),
                new OrderInfo(1002, "Ana Trujillo", "Mexico", "ANATR", "Mexico D.F."),
                new OrderInfo(1003, "Ant Fuller", "Mexico", "ANTON", "Mexico D.F."),
                new OrderInfo(1004, "Thomas Hardy", "UK", "AROUT", "London"),
                new OrderInfo(1005, "Tim Adams", "Sweden", "BERGS", "London"),
                new OrderInfo(1006, "Hanna Moos", "Germany", "BLAUS", "Mannheim"),
                new OrderInfo(1007, "Andrew Fuller", "France", "BLONP", "Strasbourg"),
                new OrderInfo(1008, "Martin King", "Spain", "BOLID", "Madrid"),
                new OrderInfo(1009, "Lenny Lin", "France", "BONAP", "Marsiella"),
                new OrderInfo(1010, "John Carter", "Canada", "BOTTM", "Lenny Lin"),
                new OrderInfo(1011, "Laura King", "UK", "AROUT", "London"),
                new OrderInfo(1012, "Anne Wilson", "Germany", "BLAUS", "Mannheim"),
                new OrderInfo(1013, "Martin King", "France", "BLONP", "Strasbourg"),
                new OrderInfo(1014, "Gina Irene", "UK", "AROUT", "London")
            };
        } 
    }
}
