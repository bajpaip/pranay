﻿using DryIoc;
using Prism.DryIoc;
using DataGridPOC.Views;
using Xamarin.Forms;
using DataGridPOC.ViewModels;

namespace DataGridPOC {
    public partial class App : PrismApplication {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized() {
            InitializeComponent();

            NavigationService.NavigateAsync("NavigationPage/MainPage");
        }

        protected override void RegisterTypes() {
            Container.RegisterTypeForNavigation<NavigationPage>();
            Container.RegisterTypeForNavigation<MainPage, MainPageViewModel>();
        }
    }
}
